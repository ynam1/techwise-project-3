class QuestionModel {
  String? question;
  Map<String, int>? answer;

  //Creating the constructor
  QuestionModel(this.question, this.answer);
}
