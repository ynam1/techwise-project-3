import 'package:flutter/material.dart';
import 'package:http/http.dart' as http; // Import the http package
import 'package:tysm/data/question_list.dart';
import 'package:tysm/Screens/result_screen.dart';
import 'package:tysm/constants.dart';

class SurveyScreen extends StatefulWidget {
  const SurveyScreen({Key? key}) : super(key: key);

  @override
  _SurveyScreenState createState() => _SurveyScreenState();
}

class _SurveyScreenState extends State<SurveyScreen> {
  List<String?> selectedOptions = List.filled(questions.length, null);
  Color mainColor = Color(0xFFFFDBD3);
  Color secondColor = Colors.white;

  PageController? _controller = PageController(initialPage: 0);

  bool isPressed = false;
  Color isSelected = tysmColor;

  int selectedIndex = -1;

  Future<void> sendSurveyData(List<String?> selectedOptions) async {
    final url = 'http://127.0.0.1:4000/add'; // Replace with your API URL

    final response = await http.post(Uri.parse(url), body: {
      'selectedOptions': selectedOptions.toString(),
    });

    if (response.statusCode == 200) {
      print('Data sent successfully!');
    } else {
      print('Failed to send data. Status code: ${response.statusCode}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      body: Padding(
        padding: EdgeInsets.all(18.0),
        child: PageView.builder(
          physics: const NeverScrollableScrollPhysics(),
          controller: _controller!,
          onPageChanged: (page) {
            setState(() {
              isPressed = false;
              selectedIndex = -1;
            });
          },
          itemCount: questions.length,
          itemBuilder: (context, index) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: double.infinity,
                  child: Text(
                    "Question ${index + 1} / ${questions.length}",
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontSize: 28.0,
                    ),
                  ),
                ),
                const Divider(
                  color: Colors.black,
                  height: 8.0,
                  thickness: 1.0,
                ),
                const SizedBox(
                  height: 20.0,
                ),
                Text(
                  questions[index].question!,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 28.0,
                  ),
                ),
                SizedBox(
                  height: 35.0,
                ),
                for (int i = 0; i < questions[index].answer!.length; i++)
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(bottom: 18.0),
                    child: MaterialButton(
                      shape: StadiumBorder(),
                      color: selectedIndex == i ? isSelected : secondColor,
                      padding: EdgeInsets.symmetric(vertical: 18.0),
                      onPressed: () {
                        setState(() {
                          selectedOptions[index] =
                              questions[index].answer!.keys.toList()[i];
                          selectedIndex = i;
                          isPressed = true;
                        });
                      },
                      child: Text(
                        questions[index].answer!.keys.toList()[i],
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                SizedBox(
                  height: 50.0,
                ),
                IconButton(
                  icon: Icon(Icons.close, color: Colors.black),
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (index > 0)
                      OutlinedButton(onPressed: () {
                        _controller!.previousPage(duration: Duration(milliseconds:500), curve: Curves.linear,
                        );
                        setState(() {
                          selectedIndex = -1;
                          isPressed = false;
                        });
                      }, 
                      style: OutlinedButton.styleFrom(
                        shape: StadiumBorder(),
                        side: BorderSide(color: Colors.grey, width: 1.0,),
                      ),
                      child: Text(
                        "Back",
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),),
                    OutlinedButton(
                      onPressed: isPressed
                          ? index + 1 == questions.length
                              ? () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ResultScreen(
                                          selectedOptions: selectedOptions),
                                    ),
                                  );
                                }
                              : () {
                                  _controller!.nextPage(
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.linear,
                                  );
                                  setState(() {
                                    isPressed = false;
                                  });
                                }
                          : null,
                      style: OutlinedButton.styleFrom(
                        shape: StadiumBorder(),
                        side: BorderSide(
                          color: Colors.orange,
                          width: 1.0,
                        ),
                      ),
                      child: Text(
                        index + 1 == questions.length
                            ? "Finish Survey"
                            : "Next Question",
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: SurveyScreen(),
  ));
}
