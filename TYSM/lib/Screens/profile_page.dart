import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tysm/Screens/Login/start_survey.dart';
import 'package:tysm/Screens/outfit_suggestion.dart'; // Import the correct path to the OutfitSuggestionScreen widget
import 'package:tysm/Screens/Login/auth_page.dart';
import 'package:tysm/Screens/profile_page.dart';

class ProfilePage extends StatefulWidget {
  final String username;

  const ProfilePage({Key? key, required this.username}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final user = FirebaseAuth.instance.currentUser!;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String? email;
  String password = '********'; // Blurred password
  bool isEditingPassword = false;
  bool isPasswordVisible = false;

  final currUser = FirebaseAuth.instance.currentUser!;
  TextEditingController newPasswordController = TextEditingController();
  void signUserOut() {
    FirebaseAuth.instance.signOut();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AuthPage(),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
 appBar: AppBar(
        centerTitle: true,
        title: SizedBox(
          height: kToolbarHeight - 15,
          child: Image.asset("assets/images/applogo.png"),
        ),
        toolbarHeight: kToolbarHeight,
        backgroundColor: Color(0xFFFFFFFF),
        foregroundColor: Colors.black,
        actions: [
          IconButton(
            onPressed: signUserOut,
            color: Colors.black,
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
              children: [
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    children: [
                      Center(
                        child: Padding(
                          padding: EdgeInsets.only(right: 47),
                          child: Align(
                            alignment: Alignment.center,
                            child: IconButton(
                              onPressed: () {
                                // Add action for profile icon
                              },
                              icon: Icon(Icons.account_circle,
                                  size: 80), // Profile icon
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 50),
                      Text(
                        'My Profile', // Title
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Email: ${user.email}',
                        style: TextStyle(fontSize: 18),
                      ),
                      SizedBox(height: 16),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Password: $password',
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () {
                              setState(() {
                                isEditingPassword = true;
                              });
                            },
                          ),
                        ],
                      ),
                      if (isEditingPassword)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 16),
                            Text(
                              'Update Password:',
                              style: TextStyle(fontSize: 18),
                            ),
                            SizedBox(height: 8),
                            TextFormField(
                              controller: newPasswordController,
                              obscureText:
                                  !isPasswordVisible, // Toggle password visibility
                              decoration: InputDecoration(
                                hintText: 'New Password',
                                suffixIcon: IconButton(
                                  icon: Icon(isPasswordVisible
                                      ? Icons.visibility_off
                                      : Icons
                                          .visibility), // Toggle icon based on password visibility
                                  onPressed: () {
                                    setState(() {
                                      isPasswordVisible =
                                          !isPasswordVisible; // Toggle password visibility
                                    });
                                  },
                                ),
                              ),
                            ),
                            SizedBox(height: 16),
                            ElevatedButton(
                              onPressed: () async {
                                try {
                                  await user.updatePassword(
                                      newPasswordController.text);
                                  setState(() {
                                    // password = newPasswordController.text;
                                    newPasswordController.clear();
                                    isEditingPassword = false;
                                  });
                                  // Show a success message or toast
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          "Password updated successfully."),
                                    ),
                                  );
                                } catch (error) {
                                  print("Error updating password: $error");
                                  // Show an error message or toast
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                          "Error updating password. Please try again."),
                                    ),
                                  );
                                }
                              },
                              child: Text('Save Password'),
                            ),
                          ],
                        ),
                    ],
                  ),
                )
              ]
        )
      ),
    );
  }
}
