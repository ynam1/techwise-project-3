import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tysm/Screens/Login/auth_page.dart';

class PastOutfitsScreen extends StatefulWidget {
  @override
  _PastOutfitsScreenState createState() => _PastOutfitsScreenState();
}

class _PastOutfitsScreenState extends State<PastOutfitsScreen> {
  final User? user = FirebaseAuth.instance.currentUser;

  void signUserOut() {
    FirebaseAuth.instance.signOut();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AuthPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: SizedBox(
          height: kToolbarHeight - 15,
          child: Image.asset("assets/images/applogo.png"),
        ),
        toolbarHeight: kToolbarHeight,
        backgroundColor: Color(0xFFFFFFFF),
        actions: [
          IconButton(
            onPressed: signUserOut,
            color: Colors.black,
            icon: Icon(Icons.logout),
          )
        ],
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection('ChatResponses')
            .doc(user!.uid)
            .collection('outfits')
            // Remove the orderBy operation
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          }

          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List<Widget> outfitWidgets = snapshot.data!.docs.map((document) {
            Map<String, dynamic> data = document.data() as Map<String, dynamic>;

            return Card(
              elevation: 3,
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: ListTile(
                contentPadding: EdgeInsets.all(15),
                title: Text(data['chatResponses']),
                subtitle: Column(
                  children: [
                    SizedBox(height: 10),
                    Image.network(
                      data['pictureSuggestion'],
                      height: 200,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(height: 10),
                    Text(data['createAt']
                        .toDate()
                        .toString()), // Display creation date
                  ],
                ),
              ),
            );
          }).toList();

          return ListView(
            children: outfitWidgets,
          );
        },
      ),
    );
  }
}
