import 'package:flutter/material.dart';
import 'package:tysm/Screens/Welcome/components/background.dart';
import 'package:tysm/Screens/Welcome/components/signup.dart';
import 'package:tysm/Screens/Login/components/text_fields.dart';
import 'package:tysm/Screens/Login/components/button.dart';
import 'package:tysm/Screens/Login/login_screen.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    final appTitle = 'Flutter Form Demo';
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        body: SignUpScreen(),
      ),
    );
  }
}

class SignUpScreen extends StatelessWidget {
  SignUpScreen({super.key});
  //text editing controllers
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  void createNewAccount() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFFFDBD3),
        body: Background(
            child: Center(
                child: Column(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: [
              const SizedBox(height: 270),

              //Text that says "Sign Up"
              Text('Sign Up',
                  style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),

              const SizedBox(height: 20),

              //Text that says First Name with text field
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'First Name',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: firstNameController,
                hintText: 'John',
                obscureText: false,
              ),
              const SizedBox(height: 20),
              //Text that says Last Name with text field
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Last Name',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: lastNameController,
                hintText: 'Smith',
                obscureText: false,
              ),
              const SizedBox(height: 20),

              //Text that says email with text field
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Email',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: emailController,
                hintText: 'design@test.com',
                obscureText: false,
              ),
              const SizedBox(height: 20),

              //Text that says create password
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Create Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: passwordController,
                hintText: 'mypassword123',
                obscureText: true,
              ),
              const SizedBox(height: 20),
              //Text that says create password
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Confirm Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              //text that says confirm password
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Confirm Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: confirmPasswordController,
                hintText: 'mypassword123',
                obscureText: true,
              ),
              const SizedBox(height: 20),

              //text that says sign up that is a button
              MyButton(
                text: "Sign Up",
                onTap: createNewAccount,
              ),
              const SizedBox(height: 25),

              //text that says "Already have account? Log in"

              GestureDetector(
                onTap: () {
                  // Navigate to the SignUp screen when tapped.
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoginScreen(
                              onTap: null,
                            )),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Already have an account?",
                        style: TextStyle(color: Colors.grey[700])),
                    const SizedBox(width: 4),
                    const Text(
                      'Log in',
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: [
                      //     Text("Don't have an account?",
                      //     style: TextStyle(color: Colors.grey[700]),),
                      //     const SizedBox(width: 4),
                      //     const Text(
                      //       'Make one',
                      //       style: TextStyle(
                      //           color: Colors.blue, fontWeight: FontWeight.bold),
                      //     )
                      //   ],
                      // )
                    )
                  ],
                ),
              ),
            ]))));
  }
}

// //Create a Form Widget
// class MyCustomForm extends StatefulWidget {
//   const MyCustomForm({super.key});

//   @override
//   MyCustomFormState createState() {
//     return MyCustomFormState();
//   }
// }

// class MyCustomFormState extends State<MyCustomForm>{
//   final _formKey = GlobalKey<FormState>();  

//   @override  
//   Widget build(BuildContext context) {  
//     // Build a Form widget using the _formKey created above.  
//     return Form(  
//       key: _formKey,  
//       child: Column(  
//         crossAxisAlignment: CrossAxisAlignment.start,  
//         children: <Widget>[  
//           TextFormField(  
//             decoration: const InputDecoration(  
//               icon: const Icon(Icons.person),  
//               hintText: 'Enter your name',  
//               labelText: 'Name',  
//             ),  
//           ),  
//           TextFormField(  
//             decoration: const InputDecoration(  
//               icon: const Icon(Icons.phone),  
//               hintText: 'Enter a phone number',  
//               labelText: 'Phone',  
//             ),  
//           ),  
//           TextFormField(  
//             decoration: const InputDecoration(  
//             icon: const Icon(Icons.calendar_today),  
//             hintText: 'Enter your date of birth',  
//             labelText: 'Dob',  
//             ),  
//            ),  
//           new Container(  
//               padding: const EdgeInsets.only(left: 150.0, top: 40.0),  
//               child: new ElevatedButton(  
//                 child: const Text('Submit'),  
//                   onPressed: null,  
//               )),  
//         ],  
//       ),  
//     );  
//   }  
// }  
  // You can define the content of your Sign Up form in this method or as a separate widget.
//   Widget _buildSignUpForm() {
//     return Center(
//         alignment: Alignment.center,        
//         child: Text('Sign Up Form'),
//     );
//   }
