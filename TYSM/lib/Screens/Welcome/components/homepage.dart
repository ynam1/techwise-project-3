import 'package:flutter/material.dart';

class BasicBottomNavBar extends StatefulWidget {
  const BasicBottomNavBar({Key? key}) : super(key: key);

  @override
  _BasicBottomNavBarState createState() => _BasicBottomNavBarState();
}

class _BasicBottomNavBarState extends State<BasicBottomNavBar> {
  int _selectedIndex = 0;

  static const List<Widget> _pages = <Widget>[
    Icon(
      Icons.diamond,
      size: 150,
    ),
    Icon(
      Icons.checkroom,
      size: 150,
    ),
    Icon(
      Icons.account_circle,
      size: 150,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle:
            true, // Align the title (app logo) in the middle of the app bar
        title: SizedBox(
          height: kToolbarHeight - 15,
          child: Image.asset("assets/images/applogo.png"),
        ),
        toolbarHeight: kToolbarHeight,
        backgroundColor: Color(0xFFFFFFFF),
      ),
      body: Center(
          // child: _pages.elementAt(_selectedIndex),
          ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xFFF3EFF5),
        selectedItemColor:
            Colors.red, //this changes the color of the selected icons to red.
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.diamond),
            label: 'Your Style',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.checkroom),
            label: 'Build',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("assets/images/dab.png"),
              size: 30,
            ),
            label: 'Profile',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.chat),
          //   label: 'Chats',
          // ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
