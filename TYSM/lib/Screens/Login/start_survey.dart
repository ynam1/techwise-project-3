import 'package:flutter/material.dart';
import 'package:tysm/Screens/survey_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';

class StartSurvey extends StatelessWidget {
  final String username;

  StartSurvey({required this.username});

  void signUserOut() {
    FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: SizedBox(
          height: kToolbarHeight - 15,
          child: Image.asset("assets/images/applogo.png"),
        ),
        toolbarHeight: kToolbarHeight,
        backgroundColor: Color(0xFFFFFFFF),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                'Welcome To The Survey, $username!',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 200),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SurveyScreen(),
                  ),
                );
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: Color(0xFFdb4c40), // Background color
                foregroundColor: Colors.white, // Text color
              ),
              child: Text(
                'START SURVEY',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: 200),
          ],
        ),
      ),
    );
  }
}
