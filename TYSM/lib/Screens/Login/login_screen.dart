//import 'dart:html';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tysm/Screens/Login/components/button.dart';
import 'package:tysm/Screens/Login/forgot_pw_page.dart';
import 'package:tysm/Screens/Welcome/components/background.dart';
import 'package:tysm/Screens/Login/components/text_fields.dart';
import 'package:tysm/Screens/Welcome/components/signup.dart';
import 'package:tysm/Screens/Welcome/components/homepage.dart';
//import 'package:tysm/Screens/Login/components/body.dart';

class LoginScreen extends StatefulWidget {
  final Function()? onTap;
  LoginScreen({super.key, required this.onTap});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //text editing controllers
  final emailController = TextEditingController();

  final passwordController = TextEditingController();

  //sign user in method
  void signUserIn() async {
    //show loading circle
    showDialog(
        context: context,
        builder: (context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });

    // try sign in
    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      // pop the loading circle
      Navigator.pop(context);
    } on FirebaseAuthException catch (e) {
      // WRONG EMAIL
      // // pop the loading circle
      Navigator.pop(context);
      showErrorMessage(e.code);
    // // pop the loading circle
    // Navigator.pop(context);
  }
  }

  // wrong email message popup
void showErrorMessage(String message) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        backgroundColor: Colors.orange,
        title: Center(
          child: Text(
            message,
            style: const TextStyle(color: Colors.white),
          ),
        ),
      );
    },
  );
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFFFDBD3),
        body: Background(
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    const SizedBox(height: 280),
              // Email Address with Email form
              // Add Email form here.
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Email Address',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: emailController,
                hintText: 'design@test.com',
                obscureText: false,
              ),
              const SizedBox(height: 20),
              // Password with password form and view password
              // Add Password form here and an option to view password.
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: passwordController,
                hintText: 'mypassword123',
                obscureText: true,
              ),
              const SizedBox(height: 15),
              // Forgot Password?
              // Add the Forgot Password? option here.
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context){
                            return ForgotPasswordPage();
                          },
                          ),
                          );
                        },
                        child: Text(
                          'Forgot Password?',
                          style: TextStyle(
                            color: Colors.grey[600],
                          ),
                        ),
                      )
                    ],
                  )),

              const SizedBox(height: 250),

              // Sign in button
              // Add the Sign in button here.
              GestureDetector(
                onTap: signUserIn,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 20), // Add spacing below the button
                  child: Container(
                    // Add any necessary styling to the container if required
                    child: Text(
                      'Sign In',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 60),
                    decoration: BoxDecoration(
                      color: Colors.red, // Set your desired button color
                      borderRadius: BorderRadius.circular(
                          30), // Set your desired border radius
                    ),
                  ),
                ),
              ),

              // Continue with Google (CAN MAYBE ADD THIS -- DONT NEED TO)
              // Add the "Continue with Google" button here.
              //             Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 25.0),
              //   child: Row(
              //     children: [
              //       Expanded(
              //         child: Divider(
              //           thickness: 0.5,
              //           color: Colors.grey[400],
              //         ),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 10.0),
              //         child: Text(
              //           'Or continue with',
              //           style: TextStyle(color: Colors.grey[700]),
              //         ),
              //       ),
              //       Expanded(
              //         child: Divider(
              //           thickness: 0.5,
              //           color: Colors.grey[400],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: const [
              //     // google button
              //     SquareTile(imagePath: 'lib/images/google.png'),

              //     SizedBox(width: 25),
              // ),

              // Make an account button (would route to create account page)
              // Add the "Make an account" button here, and set up the route to the create account page.
              GestureDetector(
                onTap: () {
                  // Navigate to the SignUp screen when tapped.
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignUp()),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Don't have an account?",
                        style: TextStyle(color: Colors.grey[700])),
                    const SizedBox(width: 4),
                    GestureDetector(
                      onTap: widget.onTap,
                      child: const Text(
                          "Register Now",
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,                          
                            ),
                        )
                      
                    ),
                  ],
                ),
              ),
            ])))));
  }
}
