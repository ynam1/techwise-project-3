//import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tysm/Screens/Login/components/button.dart';
import 'package:tysm/Screens/Welcome/components/background.dart';
import 'package:tysm/Screens/Login/components/text_fields.dart';
import 'package:tysm/Screens/Welcome/components/signup.dart';
import 'package:tysm/Screens/Welcome/components/homepage.dart';
import 'package:tysm/Screens/Login/auth_page.dart';

//import 'package:tysm/Screens/Login/components/body.dart';

class RegisterPage extends StatefulWidget {
  final Function()? onTap;
  RegisterPage({super.key, required this.onTap});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  //text editing controllers
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  //sign user up method
  void signUserUp() async {
    //show loading circle
    showDialog(
        context: context,
        builder: (context) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        });

    // try creating the user
    try {
      // check if password is confirmed
      if (passwordController.text == confirmPasswordController.text) {
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: emailController.text,
          password: passwordController.text,
        );
        final User? user = FirebaseAuth.instance.currentUser;
        final _uid = user!.uid;
        FirebaseFirestore.instance.collection('users').doc(_uid).set({
          'id': _uid,
          'email': emailController.text,
          'createAt': Timestamp.now(),
          'oid': 0,
        });
      } else {
        //show error message, passwords don't match
        Navigator.pop(context);
        showErrorMessage("Passwords don't match!");
      }
      // pop the loading circle
      Navigator.pop(context);
    } on FirebaseAuthException catch (e) {
      // WRONG EMAIL
      // // pop the loading circle
      Navigator.pop(context);
      showErrorMessage(e.code);
      // // pop the loading circle
      // Navigator.pop(context);
    }
  }

  // wrong email message popup
  void showErrorMessage(String message) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: Colors.orange,
          title: Center(
            child: Text(
              message,
              style: const TextStyle(color: Colors.white),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFFFDBD3),
        body: Background(
            child: Center(
                child: SingleChildScrollView(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
              const SizedBox(height: 280),
              // Email Address with Email form
              // Add Email form here.
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Email Address',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: emailController,
                hintText: 'design@test.com',
                obscureText: false,
              ),
              const SizedBox(height: 20),
              // Password with password form and view password
              // Add Password form here and an option to view password.
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: passwordController,
                hintText: 'mypassword123',
                obscureText: true,
              ),
              const SizedBox(height: 15),
              Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Confirm Password',
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontSize: 14,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
              MyTextField(
                controller: confirmPasswordController,
                hintText: 'Confirm Password',
                obscureText: true,
              ),
              const SizedBox(height: 200),

              // Sign in button
              // Add the Sign in button here.
              GestureDetector(
                onTap: signUserUp,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 20), // Add spacing below the button
                  child: Container(
                    // Add any necessary styling to the container if required
                    child: Text(
                      'Sign Up',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 60),
                    decoration: BoxDecoration(
                      color: Colors.red, // Set your desired button color
                      borderRadius: BorderRadius.circular(
                          30), // Set your desired border radius
                    ),
                  ),
                ),
              ),

              // Continue with Google (CAN MAYBE ADD THIS -- DONT NEED TO)
              // Add the "Continue with Google" button here.
              //             Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 25.0),
              //   child: Row(
              //     children: [
              //       Expanded(
              //         child: Divider(
              //           thickness: 0.5,
              //           color: Colors.grey[400],
              //         ),
              //       ),
              //       Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 10.0),
              //         child: Text(
              //           'Or continue with',
              //           style: TextStyle(color: Colors.grey[700]),
              //         ),
              //       ),
              //       Expanded(
              //         child: Divider(
              //           thickness: 0.5,
              //           color: Colors.grey[400],
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: const [
              //     // google button
              //     SquareTile(imagePath: 'lib/images/google.png'),

              //     SizedBox(width: 25),
              // ),

              // Make an account button (would route to create account page)
              // Add the "Make an account" button here, and set up the route to the create account page.
              GestureDetector(
                onTap: () {
                  // Navigate to the SignUp screen when tapped.
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AuthPage(),
                    ),
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Already have an account?",
                        style: TextStyle(color: Colors.grey[700])),
                    const SizedBox(width: 4),
                    GestureDetector(
                        onTap: widget.onTap,
                        child: const Text(
                          "Login Now",
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                  ],
                ),
              ),
            ])))));
  }
}
