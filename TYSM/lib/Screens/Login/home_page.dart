import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tysm/Screens/Login/start_survey.dart';
import 'package:tysm/Screens/outfit_suggestion.dart'; // Import the correct path to the OutfitSuggestionScreen widget
import 'package:tysm/Screens/Login/auth_page.dart';
import 'package:tysm/Screens/profile_page.dart';
import 'package:tysm/Screens/past_outfits.dart';

class HomePage extends StatefulWidget {
  final List<String?> selectedOptions; // Add this line

  const HomePage({Key? key, required this.selectedOptions}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final user = FirebaseAuth.instance.currentUser!;
  int _selectedIndex = 0;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String? email;
  String password = '********'; // Blurred password

  TextEditingController newPasswordController = TextEditingController();
  bool isEditingPassword = false;
  bool isPasswordVisible = false;

  // sign user out methodR
  void signUserOut() {
    FirebaseAuth.instance.signOut();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AuthPage(),
      ),
    );
  }

  static const List<Widget> _pages = <Widget>[
    Icon(
      Icons.diamond,
      size: 0,
    ),
    Icon(
      Icons.checkroom,
      size: 150,
    ),
    Icon(
      Icons.account_circle,
      size: 150,
    ),
  ];

  void _onItemTapped(int index) {
    if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => StartSurvey(
            username: user.email!, // Pass the username to StartSurvey screen
          ),
        ),
      );
    } else if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => PastOutfitsScreen(),
        ),
      );
    } else if (index == 3) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProfilePage(
            username: user.email!,
          ),
        ),
      );
    } else {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: SizedBox(
          height: kToolbarHeight - 15,
          child: Image.asset("assets/images/applogo.png"),
        ),
        toolbarHeight: kToolbarHeight,
        backgroundColor: Color(0xFFFFFFFF),
        actions: [
          IconButton(
            onPressed: signUserOut,
            color: Colors.black,
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: OutfitSuggestionScreen(selectedOptions: widget.selectedOptions),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color(0xFFF3EFF5),
        selectedItemColor:
            Colors.red, //this changes the color of the selected icons to red.
        type: BottomNavigationBarType.fixed,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.diamond),
            label: 'Your Style',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.checkroom),
            label: 'Build',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.auto_mode),
            label: 'Past Outfits',
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("assets/images/dab.png"),
              size: 30,
            ),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
