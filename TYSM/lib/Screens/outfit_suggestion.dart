import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tysm/Screens/Login/home_page.dart';
import 'package:tysm/Screens/Login/start_survey.dart';

TextStyle montserratTextStyle = TextStyle(
  fontFamily: 'Montserrat-Regular',
  fontSize: 18,
  fontWeight: FontWeight.bold,
);

class SuggestionData {
  final String outfitSuggestion;
  final String pictureSuggestion;
  SuggestionData({
    required this.outfitSuggestion,
    required this.pictureSuggestion,
  });

  factory SuggestionData.fromJson(Map<String, dynamic> json) {
    return SuggestionData(
      outfitSuggestion: json['outfit_suggestion'],
      pictureSuggestion: json['picture_suggestion'],
    );
  }
}

class OutfitSuggestionScreen extends StatefulWidget {
  final List<String?> selectedOptions;

  OutfitSuggestionScreen({required this.selectedOptions});

  @override
  _OutfitSuggestionScreenState createState() => _OutfitSuggestionScreenState();
}

class _OutfitSuggestionScreenState extends State<OutfitSuggestionScreen> {
  SuggestionData? suggestionData;
  bool isLoading = true; // Add a boolean to track loading state
  int _selectedIndex = 0;
  int latestOid = 0;
  final user = FirebaseAuth.instance.currentUser!;
  bool isRegenerating = false;

  @override
  void initState() {
    super.initState();
    fetchLatestOid();
    fetchSuggestions(); // Fetch suggestion automatically when the page loads
  }

  // Future<void> fetchSuggestion() async {
  //   try {
  //     final response = await http.post(
  //       Uri.parse('http://127.0.0.1:4000/get_outfit_suggestion'),
  //       headers: <String, String>{
  //         'Content-Type': 'application/x-www-form-urlencoded',
  //       },
  //       body: <String, String>{
  //         'selectedOptions': widget.selectedOptions.join(', '),
  //       },
  //     );

  //     if (response.statusCode == 200) {
  //       Map<String, dynamic> data = json.decode(response.body);
  //       setState(() {
  //         suggestionData = SuggestionData.fromJson(data);
  //         isLoading = false; // Data fetched, set loading to false
  //       });
  //     } else {
  //       throw Exception('Failed to load data');
  //     }
  //   } catch (e) {
  //     setState(() {
  //       isLoading = false; // Handle error, set loading to false
  //     });
  //   }
  // }
  Future<void> fetchLatestOid() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      final snapshot = await FirebaseFirestore.instance
          .collection('ChatResponses')
          .doc(user.uid)
          .collection('outfits')
          .orderBy('oid', descending: true)
          .limit(1)
          .get();

      if (snapshot.docs.isNotEmpty) {
        final data = snapshot.docs[0].data();
        setState(() {
          latestOid = data['oid'] + 1;
        });
      }
    }
  }

  Future<void> fetchSuggestions() async {
    setState(() {
      isRegenerating = true; // Set to true when regeneration starts
    });
    try {
      final response = await http.post(
        Uri.parse('http://127.0.0.1:4000/get_outfit_suggestion'),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: <String, String>{
          'selectedOptions': widget.selectedOptions.join(', '),
        },
      );
      setState(() {
        isRegenerating =
            false; // Set back to false when regeneration is complete
      });

      if (response.statusCode == 200) {
        Map<String, dynamic> data = json.decode(response.body);
        // print(data);
        // Extract the outfit suggestion and picture URLs from the response data
        String outfitSuggestion =
            data['outfit_suggestion']['choices'][0]['message']['content'];

        String pictureSuggestion = data['outfit_picture']['data'][0]['url'];

        setState(() {
          suggestionData = SuggestionData(
            outfitSuggestion: outfitSuggestion,
            pictureSuggestion: pictureSuggestion,
          );
          isLoading = false; // Data fetched, set loading to false
        });

        final User? user = FirebaseAuth.instance.currentUser;
        final _uid = user!.uid;
        //FirebaseFirestore.instance.collection('users').doc(_uid).get('oid');

        FirebaseFirestore.instance
            .collection('ChatResponses')
            .doc(_uid)
            .collection('outfits')
            .doc(latestOid.toString())
            .set({
          'id': _uid,
          'oid': latestOid,
          'chatResponses': suggestionData!.outfitSuggestion,
          'pictureSuggestion': suggestionData!.pictureSuggestion,
          'createAt': Timestamp.now(),
        });

        setState(() {
          latestOid++;
        });
      } else {
        throw Exception('Failed to load data');
      }
    } catch (e) {
      setState(() {
        isLoading = false; // Handle error, set loading to false
        isRegenerating = false;
      });
    }
  }

  // Future<void> fetchSuggestion() async {
  //   try {
  //     final response = await http.post(
  //       Uri.parse('http://127.0.0.1:4000/get_outfit_picture'),
  //       headers: <String, String>{
  //         'Content-Type': 'application/x-www-form-urlencoded',
  //       },
  //       body: <String, String>{
  //         'selectedOptions': widget.selectedOptions.join(', '),
  //       },
  //     );

  //     if (response.statusCode == 200) {
  //       Map<String, dynamic> data = json.decode(response.body);
  //       // Extract the image URL from the 'data' field
  //       String imageUrl = data['outfit_suggestion']['data'][0]['url'];

  //       setState(() {
  //         suggestionData = SuggestionData(
  //           outfitSuggestion:
  //               imageUrl, // Use the image URL as the outfit suggestion
  //         );
  //         isLoading = false; // Data fetched, set loading to false
  //       });
  //       final User? user = FirebaseAuth.instance.currentUser;
  //       final _uid = user!.uid;
  //       int _oid = 0;
  //       //FirebaseFirestore.instance.collection('users').doc(_uid).get('oid');

  //       FirebaseFirestore.instance
  //           .collection('ChatResponses')
  //           .doc(_uid)
  //           .collection('outfits')
  //           .doc(_oid.toString())
  //           .set({
  //         'id': _uid,
  //         'oid': _oid,
  //         'chatResponses': suggestionData!.outfitSuggestion,
  //         'createAt': Timestamp.now(),
  //       });
  //       _oid++;
  //     } else {
  //       throw Exception('Failed to load data');
  //     }
  //   } catch (e) {
  //     setState(() {
  //       isLoading = false; // Handle error, set loading to false
  //     });
  //   }
  // }

  void _onItemTapped(int index) {
    if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => StartSurvey(
            username: user.email!, // Pass the username to StartSurvey screen
          ),
        ),
      );
    } else {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (isLoading) CircularProgressIndicator(),
            if (!isLoading && suggestionData != null)
              Column(children: [
                Text(
                  'OUTFIT SUGGESTION',
                  style: montserratTextStyle,
                ),
                SizedBox(height: 40), // Add more spacing
                Container(
                  width: 300, // Adjust the width as needed
                  height: 300, // Adjust the height as needed
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      suggestionData!
                          .pictureSuggestion, // Use the imageURL here
                      width: double.infinity,
                      height: double.infinity,
                      fit: BoxFit.cover,
                      loadingBuilder: (BuildContext context, Widget child,
                          ImageChunkEvent? loadingProgress) {
                        if (loadingProgress == null) return child;
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                    loadingProgress.expectedTotalBytes!
                                : null,
                          ),
                        );
                      },
                      errorBuilder: (BuildContext context, Object error,
                          StackTrace? stackTrace) {
                        return Center(
                          child: Text('Error loading image'),
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 50),
                Container(
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    suggestionData!.outfitSuggestion,
                    style: montserratTextStyle.copyWith(
                      fontSize: 16,
                      color: Colors.black87,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 40),
                ElevatedButton(
                  onPressed: isRegenerating
                      ? null // Disable the button if regenerating
                      : () {
                          // Enable regenerating
                          setState(() {
                            isRegenerating = true;
                          });
                          fetchSuggestions().then((_) {
                            // After fetch is complete, reset regeneration state
                            setState(() {
                              isRegenerating = false;
                            });
                          });
                        },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color(0xFFdb4c40),
                    foregroundColor: Colors.white,
                  ),
                  child: Text(
                    isRegenerating
                        ? 'Regenerating...'
                        : 'Regenerate', // Update button text
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ]),
          ]),
    )));
  }
}
