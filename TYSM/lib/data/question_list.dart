import 'package:tysm/model/question_model.dart';

List<QuestionModel> questions = [
  QuestionModel(
    "What is your height?",
    {
      "4'11 and under": 0,
      "5'0~5'5": 1,
      "5'6~5'9": 2,
      "5'9~6'0": 3,
      "6'0~6'4": 4,
      "6'5 onwards": 5,
    },
  ),
  QuestionModel(
    "What is your weight?",
    {
      "100 lbs and below": 0,
      "100 lbs - 125 lbs": 1,
      "126 lbs - 150 lbs": 2,
      "151 lbs - 175 lbs": 3,
      "176 lbs - 200 lbs": 4,
      "200 lbs onwards": 5,
    },
  ),
  QuestionModel(
    "What is your preferred material?",
    {
      "Cotton": 0,
      "Polyester": 1,
      "Nylon": 2,
      "Don't Care": 3,
    },
  ),
  QuestionModel(
    "What type of color fits you most?",
    {
      "Neutral": 0,
      "Vibrant": 1,
      "Dark": 2,
      "Earthy": 3,
    },
  ),
  QuestionModel(
    "What is your preferred style?",
    {
      "Punk/Goth": 0,
      "Street": 1,
      "Casual": 2,
      "Business": 3,
      "Athletic": 4,
    },
  ),
];
