import 'package:flutter/material.dart';

class MontserratFontApp extends StatelessWidget {
  final Widget child;

  MontserratFontApp({required this.child});

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        return MaterialApp(
          builder: (BuildContext context, Widget? child) {
            return DefaultTextStyle(
              style: TextStyle(fontFamily: 'Montserrat-Regular'),
              child: child!,
            );
          },
          home: child,
        );
      },
    );
  }
}
