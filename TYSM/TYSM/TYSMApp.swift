//
//  TYSMApp.swift
//  TYSM
//
//  Created by Chris Nam on 7/19/23.
//

import SwiftUI

@main
struct TYSMApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
