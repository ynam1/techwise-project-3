#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
import characteristics
import os
import openai
from dotenv import load_dotenv

def getQuestion(height, weight, material, color, style):
    if material == "Don't Care":
        return ("Suggest me a outfit for someone who is "  + height + ", " + weight + " , and " + "who likes " + style + " style, " + color + " colors, and they don't care about the material used. In 2 sentences.")
    return ("Suggest me a outfit for someone who is "  + height + ", " + weight + " , and " + "who likes " + style + " style, " + color + " colors, and they like " + material + ".In 2 sentences.")
def getSuggest(height, weight, material, color, style):
    load_dotenv()
    question = getQuestion(height, weight, material, color, style)
    openai.api_key = os.getenv("API_KEY")
    response = openai.ChatCompletion.create(
        model = "gpt-3.5-turbo",
        messages = [
            {"role": "user", "content": question}
        ]
    )

    print(response.choices[0].message.content)