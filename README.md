# TechWise Project 3 - Fashion Suggestion App

Over the course of two months, we developed a mobile app focused on advising personalized fashion tips for users.

Users will be able to personalize their suggestions by selecting their desired style, bodily characteristics, and even clothing material. 

With these categories, the app connects the provided information with an API in order to provide users with the best style.

