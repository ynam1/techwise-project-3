from flask import Flask, request, jsonify
import ast
import preference_model
import os
import openai
from dotenv import load_dotenv

app = Flask(__name__)

# Load environment variables from .env
load_dotenv()

# OpenAI API Key
openai.api_key = os.getenv("API_KEY")

# @app.route('/get_outfit_suggestion', methods=['POST'])
# def get_outfit_suggestion():
#     data_string = request.form.get('selectedOptions')
    
#     # Remove the enclosing square brackets and then split using comma
#     data_list = data_string[0:-1].split(', ')

#     height, weight, material, color, style = data_list
    
#     # Generate the question for OpenAI
#     question = preference_model.getQuestion(height, weight, material, color, style)

#     print(question)
    
#     # Create a message for OpenAI
#     messages = [{"role": "user", "content": question}]
    
#     # Call the OpenAI API
#     response = openai.ChatCompletion.create(
#         model="gpt-3.5-turbo",
#         messages=messages
#     )
    
#     # Get the OpenAI response
#     outfit_suggestion = response.choices[0].message.content
    
#     # Return the OpenAI response as JSON
#     return jsonify({"outfit_suggestion": outfit_suggestion})

@app.route('/get_outfit_suggestion', methods=['POST'])
def get_outfit_suggestion():
    data_string = request.form.get('selectedOptions')
    
    # Remove the enclosing square brackets and then split using comma
    data_list = data_string[0:-1].split(', ')

    height, weight, material, color, style = data_list
    
    # Generate the question for OpenAI
    question = preference_model.getQuestion(height, weight, material, color, style)

    print(question)
    
    # Create a message for OpenAI
    messages = [{"role": "user", "content": question}]
    
    # Call the OpenAI API
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=messages
    )
    
    # Get the OpenAI response
    outfit_suggestion = response.choices[0].message.content

    image = openai.Image.create(
    prompt=outfit_suggestion,
    n=1,
    size="256x256",

)
    print(image)
    # Return the OpenAI response as JSON
    return jsonify({"outfit_suggestion": response, "outfit_picture": image})


if __name__ == "__main__":
    app.run(port=4000, debug=True)
