import tensorflow as tf
import numpy as np

# Define the training data
input_data = np.array([
    [0, 0, 1, 0, 1, 1, 0, 0],  # User input features (e.g., weather, occasion, etc.)
    [1, 0, 0, 1, 0, 1, 1, 0],
    [0, 1, 1, 0, 1, 0, 0, 1],
    [1, 1, 0, 1, 1, 0, 1, 0],
    [1, 0, 1, 1, 0, 1, 0, 1],
    [0, 1, 0, 0, 1, 0, 1, 1],
    [0, 1, 1, 1, 0, 1, 1, 0],
    [1, 0, 1, 0, 1, 0, 1, 1]
])
output_data = np.array([
    [1, 0, 0, 0, 0, 0, 0],  # Suggested clothes (e.g., T-shirt, jeans, etc.)
    [0, 1, 0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 1]
])

# Define the model architecture
model = tf.keras.Sequential([
    tf.keras.layers.Dense(32, activation='relu', input_shape=(8,)),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(7, activation='softmax')
])

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(input_data, output_data, epochs=100)

# User input for inference
weather = int(input("Enter weather (0 for cold, 1 for warm): "))
occasion = int(input("Enter occasion (0 for casual, 1 for formal): "))
season = int(input("Enter season (0 for non-winter, 1 for winter): "))
style = int(input("Enter style preference (0 for casual, 1 for formal): "))
pattern = int(input("Enter pattern preference (0 for plain/solid, 1 for patterned): "))
color = int(input("Enter color preference (0 for neutral, 1 for vibrant): "))
length = int(input("Enter length preference (0 for short, 1 for long): "))
fit = int(input("Enter fit preference (0 for loose, 1 for fitted): "))

user_input = np.array([[weather, occasion, season, style, pattern, color, length, fit]])

# Make predictions
predictions = model.predict(user_input)

# Print the suggested clothes
clothes = ['T-shirt', 'Jeans', 'Dress', 'Shorts', 'Skirt', 'Sweater', 'Pants']
suggested_clothes = [clothes[i] for i in np.argmax(predictions, axis=1)]
print("Suggested clothing:", suggested_clothes[0])
